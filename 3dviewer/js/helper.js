///////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// helper.js
// ヘルパ&補助情報表示クラス
// ver 0.83
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Helper
ThreeJsHelper = function()
{
	// 基本的な幅。
	// 緯度経度の最大値から再計算し直す必要があります。
	this.max = 9;
};

///////////////////////////////////////////////////////////////////////////////
ThreeJsHelper.prototype =
{
	constructor: ThreeJsHelper,
	///////////////////////////////////////////////////////////////////////////////
	// ヘルバ
	stats : function()
	{
		// Stats
		TerraDroneViewer.stats = new Stats();
		TerraDroneViewer.stats.setMode(0);
		TerraDroneViewer.stats.domElement.style.position = "absolute";
		TerraDroneViewer.stats.domElement.style.left = "0px";
		TerraDroneViewer.stats.domElement.style.top = "0px";
		document.body.appendChild(TerraDroneViewer.stats.domElement);
	},

	///////////////////////////////////////////////////////////////////////////////
	// 単体文字生成
	// @param	string		表示したい文字
	// @param	pos			配置する座標
	createStr : function(string, pos, rot)
	{
		var geometry =
			new THREE.TextGeometry(
				string,
				{
					size: 0.2,
					height: 0.01,
					font: "helvetiker",
					bevelThickness: 1,
					bevelSize: 0,
					bevelEnabled: false
		});
		var material = new THREE.MeshBasicMaterial({ color: 0xffffff });
		var textObj = new THREE.Mesh(geometry, material);
		textObj.position.set(pos.x, pos.y, pos.z);
		textObj.rotation.set(rot.x, rot.y, rot.z);
		return textObj;
	},

	// XYZのアクシズ
	axis : function()
	{
		// Axis
		var axis = new THREE.AxisHelper(3);
		axis.position.set(-this.max, -this.max, TerraDroneViewer.system.base_z);
		TerraDroneViewer.scene.add(axis);
	},

	// 緯度経度
	// もしこれが将来負荷の元になるのならCanvasに変更
	latilong : function()
	{
//		var material = new THREE.MeshLambertMaterial({ color: 0xffffff });
		var material = new THREE.MeshBasicMaterial({ color: 0xffffff });
		var geometry = new THREE.Geometry();

		// センター緯度
		var lat_center = new THREE.Vector3(this.max + 1, 0, TerraDroneViewer.system.base_z);
		var rotation = new THREE.Vector3(0, 0, 0);
		// 差分を出します
		var diff = Math.abs(TerraDroneViewer.params.source.north > TerraDroneViewer.params.source.south);
		var one_scale = diff / 20;
		for (var y = -this.max; y < this.max + 1; y ++)
		{
			lat_center.y = y;
			// 数値を計算します
			var lat = TerraDroneViewer.params.source.north + (one_scale * y);
			// 小数点の桁を合わせます。
			lat = lat.toFixed(TerraDroneViewer.system.latlng_digit);
			// メッシュを生成します
			var mesh = this.createStr(lat, lat_center, rotation);
			geometry.mergeMesh(mesh);
		}

		// センター経度
		var lng_center = new THREE.Vector3(0, -(this.max + 1), TerraDroneViewer.system.base_z);
		rotation = new THREE.Vector3(0, 0, -90);
		// 差分を出します
		var diff = Math.abs(TerraDroneViewer.params.source.east > TerraDroneViewer.params.source.west);
		var one_scale = diff / 20;
		for (var x = -this.max; x < this.max + 1; x ++)
		{
			lng_center.x = x;
			// 数値を計算します
			var lng = TerraDroneViewer.params.source.east + (one_scale * x);
			// 小数点の桁を合わせます。
			lng = lng.toFixed(TerraDroneViewer.system.latlng_digit);
			// メッシュを生成します
			var mesh = this.createStr(lng, lng_center, rotation);
			geometry.mergeMesh(mesh);
		}


		var textObj = new THREE.Mesh(geometry, material);
		TerraDroneViewer.scene.add(textObj);

	},

	///////////////////////////////////////////////////////////////////////////////
	// グリッドライン
	grid : function()
	{
		var grid = new THREE.GridHelper(this.max, 1);
		grid.rotation.x = -90 * Math.PI / 180;
		grid.position.z = TerraDroneViewer.system.base_z;
		TerraDroneViewer.scene.add(grid);

		// 疑似等高線
		for (var y = TerraDroneViewer.system.base_z; y < (TerraDroneViewer.system.base_z + 1.25); y += 0.25)
		{
			var geometry = new THREE.Geometry();

			geometry.vertices.push(new THREE.Vector3( -this.max, -this.max, y)); 
			geometry.vertices.push(new THREE.Vector3( -this.max, this.max, y)); 
			geometry.vertices.push(new THREE.Vector3( this.max, this.max, y)); 

			var line = new THREE.Line( geometry, new THREE.LineBasicMaterial({color: 0xffffff}));
			TerraDroneViewer.scene.add( line );
		}
	},

	////////////////////////////////////////////////////////////////////////////////
	// スピナーを表示
	showSpinner : function()
	{
		//設定
		var option = {
			lines: 12, //線の数
			length: 20, //線の長さ
			width: 11, //線の幅
			radius: 16, //スピナーの内側の広さ
			corners: 1, //角の丸み
			rotate: 74, //向き(あんまり意味が無い・・)
			direction: 1, //1：時計回り -1：反時計回り
			color: '#ffffff', // 色
			speed: 1.0, // 一秒間に回転する回数
			trail: 71, //残像の長さ
			shadow: true, // 影
			hwaccel: true, // ？
			className: 'spinner', // クラス名
			zIndex: 2e9, // Z-index
			top: '50%', // 相対 TOP
			left: '50%', // 相対 LEFT
			opacity: 0.25, //透明度
			fps: 20 //fps
			};
		var target = document.getElementById('dialogue');
		TerraDroneViewer.spinner = new Spinner(option).spin(target);

		console.log("スピナーで待ち状態を開始します");
	},

	////////////////////////////////////////////////////////////////////////////////
	// スピナーを消去
	hideSpinner : function()
	{
		// 動きを止めます
		TerraDroneViewer.spinner.spin();

		console.log("スピナーを停止します");
	}

};

