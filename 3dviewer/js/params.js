////////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// params.js
// 引数処理関連
// ver 0.9
// UTF-8/CR+LF/TAB:4
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// 自分のアドレスのURLと引数をチェックする関数
TerraDroneViewer.func.parseQuery = function()
{
	// URL
	var hostname = window.location.hostname;
	console.log("コールされたドメイン=" + hostname);

	// 引数厳密チェックを行います
	if (document.location.search == "")
	{
		console.error("引数が存在しません。コールアドレス=" + full_query);
		return false;
	}

	// 引数を取り出します。
	var full_query = document.location.search.substring(1);
	if (full_query == "")
	{
		console.error("引数が存在しません。コールアドレス=" + full_query);
		return false;
	}

	// 空白その他を省くためにURLデコードします
	full_query = decodeURIComponent(full_query);

	///////////////////////////////////////////////////
	// 想定する引数：
	// １個の場合
	// ex) ***.html?project=47&source_date=2017-02-28
	// 
	// ２個の場合
	// ex) ***.html?project=47&source_date=2017-02-28&target_date=2017-03-01&coordinate=[[15.9985806,%20108.2466817],[15.9984237,%20108.2468403],[15.9984262,%20108.2465959]]
	///////////////////////////////////////////////////

	// 引数を&で分解し、総数をチェックします
	var params = full_query.split('&');
	console.log(params + " " + params.length);
	switch(params.length)
	{
		// 配列が1個（引数が1個）
		case 1:
			// 表示モード
			console.log("単体表示モードで動作させます");
			// ビューモードを設定します
			TerraDroneViewer.system.viewMode = 1;
			// シーケンス最大数を１にします（ロード時の表示：n/1）
			TerraDroneViewer.sequence.max = 1;

			/////////////////////////////////////////////
			// TODO:
			// 厳密に判定するなら変数名をチェックする事

			// プロジェクトID
			var vars = params[0];
			var elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.project = elements[1];

			// 使わない変数はエラー防止代入をしておきます。
			TerraDroneViewer.params.source.date = null;
			TerraDroneViewer.params.target.date = null;
			TerraDroneViewer.params.coordinate = null;

			// 配列にも入れておきます
			TerraDroneViewer.dates[0] = "Latest";
			TerraDroneViewer.dates[1] = null;

			break;

		// 配列が2個（引数が２個）
		case 2:
			// 表示モード
			console.log("単体表示モードで動作させます");
			// ビューモードを設定します
			TerraDroneViewer.system.viewMode = 1;
			// シーケンス最大数を１にします（ロード時の表示：n/1）
			TerraDroneViewer.sequence.max = 1;

			/////////////////////////////////////////////
			// TODO:
			// 厳密に判定するなら変数名をチェックする事

			// プロジェクトID
			var vars = params[0];
			var elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.project = elements[1];

			// 日付1
			vars = params[1];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.source.date = elements[1];

			// 使わない変数はエラー防止代入をしておきます。
			TerraDroneViewer.params.target.date = null;
			TerraDroneViewer.params.coordinate = null;

			// 配列にも入れておきます
			TerraDroneViewer.dates[0] = "Latest";
			TerraDroneViewer.dates[1] = null;

			break;

		// 配列が4個（引数が４個）
		case 3:
			// 2017/05/08
			// coordinateという値をsurface_compareAPIに渡さないとエラーが発生していました
			// 引数には意味が無いことが判明したため、存在しなくても動くように修正します
			// 但しサーバ側の対応が必要です

			console.log("２個表示モードで動作させます");
			// ビューモードを設定します
			TerraDroneViewer.system.viewMode = 2;
			// シーケンス最大数を２にします（ロード時の表示：n/2）
			TerraDroneViewer.sequence.max = 2;

			/////////////////////////////////////////////
			// TODO:
			// 厳密に判定するなら変数名をチェックする事

			// プロジェクトID
			var vars = params[0];
			var elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.project = elements[1];

			// 日付1
			vars = params[1];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.source.date = elements[1];

			// 日付2
			vars = params[2];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.target.date = elements[1];

			// body変数
			TerraDroneViewer.params.coordinate = "";

			// 配列にも入れておきます
			TerraDroneViewer.dates[0] = TerraDroneViewer.params.source.date;
			TerraDroneViewer.dates[1] = TerraDroneViewer.params.target.date;

			break;

		// 配列が4個（引数が４個）
		case 4:
			console.log("２個表示モードで動作させます");
			// ビューモードを設定します
			TerraDroneViewer.system.viewMode = 2;
			// シーケンス最大数を２にします（ロード時の表示：n/2）
			TerraDroneViewer.sequence.max = 2;

			/////////////////////////////////////////////
			// TODO:
			// 厳密に判定するなら変数名をチェックする事

			// プロジェクトID
			var vars = params[0];
			var elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.project = elements[1];

			// 日付1
			vars = params[1];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.source.date = elements[1];

			// 日付2
			vars = params[2];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.target.date = elements[1];

			// body変数
			vars = params[3];
			elements = vars.split('=');
			// 変数に入れ込みます。
			TerraDroneViewer.params.coordinate = elements[1];

			// 配列にも入れておきます
			TerraDroneViewer.dates[0] = TerraDroneViewer.params.source.date;
			TerraDroneViewer.dates[1] = TerraDroneViewer.params.target.date;

			break;

		default:
			console.error("引数が正しくありません。コールアドレス=" + full_query);
			return false;
	}

	return true;
};

