///////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// geoAnalyze.js
// 共通定義
// ver 0.8
// UTF-8/CR+LF/TAB:4
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// 不明点：
// 北緯・南緯の情報の相違はどういう値で来るのか
// 現在はプラス側の浮動小数点しか存在しないため、南北情報の追加が必要と思われる。
// 情報が無いため、南北の情報が逆転することで判定するものと仮定し実装

///////////////////////////////////////////////////////////////////////////////
// 解析機能初期化関数
TerraDroneViewer.func.geoAnalyzeInit = function()
{
	// 角材の幅
	TerraDroneViewer.analyze.diff_width = 0.2;
	TerraDroneViewer.analyze.diff_height = 0.2;

	// 線分の節々にある球体のサイズ
	TerraDroneViewer.analyze.sphere_diameter = 0.1;
};

///////////////////////////////////////////////////////////////////////////////
// 緯度経度を座標変換するための「事前」計算
// KMLとplyが終わった時点でコールする必要があります。
TerraDroneViewer.func.standby4ConvertPosition = function()
{
	// TODO:
	// 毎回判定はコストが高いので構造化
	if (TerraDroneViewer.params.source.north == undefined)
	{
		console.log("KMLから緯度経度を取得しないと実行出来ません");
		return false;
	}
	if (TerraDroneViewer.params.source.area.x_min == undefined)
	{
		console.log("plyのロードが完了していないと実行出来ません");
		return false;
	}

	// 緯度の範囲を生成します。
	if (TerraDroneViewer.params.source.north > TerraDroneViewer.params.source.south)
	{
		// 差分を出します
		TerraDroneViewer.analyze.lat_height = TerraDroneViewer.params.source.north - TerraDroneViewer.params.source.south;
		// 緯度の中央を出します。
		TerraDroneViewer.analyze.lat_center = TerraDroneViewer.params.source.south + (TerraDroneViewer.analyze.lat_height / 2);
		// 北半球フラグを立てます
		TerraDroneViewer.analyze.is_north = true;
	}
	else
	{
		// 差分を出します
		TerraDroneViewer.analyze.lat_height = TerraDroneViewer.params.source.south - TerraDroneViewer.params.source.north;
		// 緯度の中央を出します。
		TerraDroneViewer.analyze.lat_center = TerraDroneViewer.params.source.north + (TerraDroneViewer.analyze.lat_height / 2);
		// 南半球フラグを立てます
		TerraDroneViewer.analyze.is_north = false;
	}
	// 経度の範囲を生成します。
	if (TerraDroneViewer.params.source.east > TerraDroneViewer.params.source.west)
	{
		// 差分を出します
		TerraDroneViewer.analyze.lng_width = TerraDroneViewer.params.source.east - TerraDroneViewer.params.source.west;
		// 経度の中央を出します。
		TerraDroneViewer.analyze.lng_center = TerraDroneViewer.params.source.west + (TerraDroneViewer.analyze.lng_width / 2);
		// 東経フラグを立てます
		TerraDroneViewer.analyze.is_east = true;
	}
	else
	{
		// 差分を出します
		TerraDroneViewer.analyze.lng_width = TerraDroneViewer.params.source.west - TerraDroneViewer.params.source.east;
		// 経度の中央を出します。
		TerraDroneViewer.analyze.lng_center = TerraDroneViewer.params.source.east + (TerraDroneViewer.analyze.lng_width / 2);
		// 西経フラグを立てます
		TerraDroneViewer.analyze.is_east = false;
	}

	// plyの座標範囲を生成します。
	TerraDroneViewer.analyze.ply_width = TerraDroneViewer.params.source.area.x_max - TerraDroneViewer.params.source.area.x_min;
	TerraDroneViewer.analyze.ply_height = TerraDroneViewer.params.source.area.y_max - TerraDroneViewer.params.source.area.y_min;
	TerraDroneViewer.analyze.ply_up = TerraDroneViewer.params.source.area.z_max - TerraDroneViewer.params.source.area.z_min;

	// 変換準備完了フラグを立てます。
	TerraDroneViewer.analyze.standby_conv_pos = true;

	return true;
};

////////////////////////////////////////////////////////////////////////////////
// 体積計算のアドレス取得
// @param	project		プロジェクトID
TerraDroneViewer.func.createCalcVolumeUrl = function(project)
{
	var url = TerraDroneViewer.url.calc_volume.replace(TerraDroneViewer.macro.project, project);
	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

///////////////////////////////////////////////////////////////////////////////
// 描画座標から緯度経度に変換する関数
// @pre		KMLがロード完了してないと判断できません
// @pre		Sourceのplyがロード完了してないと判断できません
TerraDroneViewer.func.convXY2LatLng = function(x, y)
{
};

///////////////////////////////////////////////////////////////////////////////
// 緯度経度から描画座標に変換する関数
// @pre		KMLがロード完了してないと判断できません
// @pre		Sourceのplyがロード完了してないと判断できません
TerraDroneViewer.func.convLatLng2XY = function(latitude, longitude)
{
	// 事前計算が終わっていなければコールできません
	if (!TerraDroneViewer.analyze.standby_conv_pos)
	{
		console.error("KMLロードとply描画が未完了状態ではコールできません");
		return false;
	}
	// もし引数が範囲を超えたいたら失敗です
	if (TerraDroneViewer.analyze.is_north)
	{
		if ((latitude > TerraDroneViewer.params.source.north) ||
			(TerraDroneViewer.params.source.south > latitude))
		{
			console.error("緯度がKMLで指定された範囲を超えています\n" +
			"KML lat north = " + TerraDroneViewer.params.source.north +
			" south = " + TerraDroneViewer.params.source.south);
			return false;
		}
	}
	else
	{
		if ((latitude > TerraDroneViewer.params.source.south) ||
			(TerraDroneViewer.params.source.north > latitude))
		{
			console.error("緯度がKMLで指定された範囲を超えています\n" +
			"KML lat north = " + TerraDroneViewer.params.source.north +
			" south = " + TerraDroneViewer.params.source.south);
			return false;
		}
	}
	if (TerraDroneViewer.analyze.is_east)
	{
		if ((longitude > TerraDroneViewer.params.source.east) ||
			(TerraDroneViewer.params.source.west > longitude))
		{
			console.error("経度がKMLで指定された範囲を超えています\n" +
			"KML lat east = " + TerraDroneViewer.params.source.east +
			" west = " + TerraDroneViewer.params.source.west);
			return false;
		}
	}
	else
	{
		if ((longitude > TerraDroneViewer.params.source.west) ||
			(TerraDroneViewer.params.source.east > longitude))
		{
			console.error("経度がKMLで指定された範囲を超えています\n" +
			"KML lat east = " + TerraDroneViewer.params.source.east +
			" west = " + TerraDroneViewer.params.source.west);
			return false;
		}
	}

	// 位置計算
	// 引数の緯度を
//	if (latitude

};

///////////////////////////////////////////////////////////////////////////////
// 断面解析用データ表示：線で引くバージョン
// ※WindowsではWebGLミドルウェアの仕様上線が太くなりません。
// 節の位置に球体を配置します。
// id:管理ID
// points:緯度、経度の配列
TerraDroneViewer.func.showSectionalLine = function(id, points)
{
	// テストでチューブをコール
//	return TerraDroneViewer.func.showSectionalTube(id, points);

	///////////////////////////////////////////////////////////////////////////
	// デモ専用実装

	// ダミーを初期化します。
	TerraDroneViewer.func.initDummy();
	var material = new THREE.LineBasicMaterial({color: 0xffffff, linewidth: 5});

	// ジオメトリを繋ぎます。
	var geometry = new THREE.Geometry();
	for (var i = 0; i < TerraDroneViewer.dummy.lines.length; i ++)
	{
		var pos = TerraDroneViewer.dummy.lines[i];
		var x = pos[0];
		var y = pos[1];
		var z = pos[2] + 0.5;
		geometry.vertices.push( new THREE.Vector3(x, y, z)); 
	}
/*
	geometry.vertices.push( new THREE.Vector3( -4, 3.6, -4.4 + 0.5)); 
	geometry.vertices.push( new THREE.Vector3( 0, 0, -4.5 + 0.5)); 
	geometry.vertices.push( new THREE.Vector3( 2, 3, -4.47 + 0.5)); 
	geometry.vertices.push( new THREE.Vector3( 2.3, 2.6, -4.72 + 0.5));
	geometry.vertices.push( new THREE.Vector3( 2.7, 1.3, -4.5 + 0.5));
	geometry.vertices.push( new THREE.Vector3( 6.0, -5.5, -4.6 + 0.5));
*/
	// ラインメッシュを生成します。
	var line = new THREE.Line(geometry, material);
	TerraDroneViewer.scene.add( line );

	////////////////////////////////////
	// 節々に球を配置していきます

	var mat = new THREE.MeshLambertMaterial({color: 0xffffff});
	var geo = new THREE.Geometry();
	var len = TerraDroneViewer.dummy.lines.length;
	for (var i = 0; i < len ; i ++)
	{
		var g = new THREE.SphereGeometry(TerraDroneViewer.analyze.sphere_diameter, 32, 32);
		var m = new THREE.Mesh(g, mat);
		var pos = TerraDroneViewer.dummy.lines[i];
		m.position.set(pos[0], pos[1], pos[2] + 0.5);
		TerraDroneViewer.scene.add(m);
	}

	return;

	///////////////////////////////////////////////////////////////////////////
	// 本番用実装

	// plyから最大値のzが取れていない場合は失敗とします。
	if (TerraDroneViewer.params.source.area.z_max == null)
	{
		console.error("ply描画が未完了状態ではコールできません");
		return false;
	}

	var geometry = new THREE.Geometry();
	var len = ponts.length;
	for (var i = 0; i < len; i ++)
	{
		geometry.vertices[i] = new THREE.Vector3(points[0], points[1], points[2]);
	}
	// ラインメッシュを生成します。
	var line = new THREE.Line( geometry, new THREE.LineBasicMaterial({color: 0xffffff, linewidth: 5}));
	TerraDroneViewer.scene.add( line );

};

///////////////////////////////////////////////////////////////////////////////
// 断面解析用データ表示：チューブで引くバージョン
// チューブは恐らく使えない
// 理由１：節を増やさないと正しい座標で関節を生成してくれない
// 理由２：関節を増やすとベジエ曲線のようになってしまう
// id:管理ID
// points:緯度、経度の配列
TerraDroneViewer.func.showSectionalTube = function(id, points)
{
	var path = new THREE.SplineCurve3([
		new THREE.Vector3( -4, 3.6, -4.4),
		new THREE.Vector3( 0, 0, -4.5),
		new THREE.Vector3( 2, 3, -4.47),
		new THREE.Vector3( 2.3, 2.6, -4.72),
		new THREE.Vector3( 2.7, 1.3, -4.5),
		new THREE.Vector3( 6.0, -5.5, -4.6)
	]);
//	var material = new THREE.MeshBasicMaterial({color: 0xffffff, transparent: true, opacity: 0.9});
	var material = new THREE.MeshLambertMaterial({color: 0xffffff, transparent: true, opacity: 0.9});
	var geometry = new THREE.TubeGeometry(path, 24, 0.05, 8, false, false);
	var tube = new THREE.Mesh(geometry, material);
	tube.castShadow = true;
	TerraDroneViewer.scene.add(tube);

	////////////////////////////////////
	// 節々に球を配置していきます

	TerraDroneViewer.func.initDummy();
	var mat = new THREE.MeshLambertMaterial({color: 0xffffff});
	var geo = new THREE.Geometry();

	var len = TerraDroneViewer.dummy.lines.length;
	for (var i = 0; i < len ; i ++)
	{
		var g = new THREE.SphereGeometry(TerraDroneViewer.analyze.sphere_diameter, 32, 32);
		var m = new THREE.Mesh(g, mat);
		var pos = TerraDroneViewer.dummy.lines[i];
		m.position.set(pos[0], pos[1], pos[2]);
		TerraDroneViewer.scene.add(m);
	}

};

///////////////////////////////////////////////////////////////////////////////
// 断面解析用データ消去
// id:管理ID
TerraDroneViewer.func.hideSectionalLine = function(id)
{
	
};

///////////////////////////////////////////////////////////////////////////////
// カメラから真下に光線を投げてZ位置を計算
TerraDroneViewer.func.getRaycastZpos = function(x, y)
{
	var raycaster = new THREE.Raycaster();
	var mouse = {};
	mouse.x = 0;
	mouse.y = 0;

	// カメラをその位置の上空に移動させます
	TerraDroneViewer.camera2.position.set(x, y, 5);
	// 真下に向けます。
	TerraDroneViewer.camera2.lookAt(new THREE.Vector3(0, 0, 0));
	// カメラをレイキャストにセットします
	raycaster.setFromCamera( mouse, TerraDroneViewer.camera2 );
	// オブジェクトの取得
	var intersects = raycaster.intersectObjects( TerraDroneViewer.maps.children );

	if (intersects.length != 0)
	{
		var z = intersects[0].point.z;
		z = z.toFixed(10);
		console.log("Intersect Z = " + z);
		return z;
	}
	return null;
/*
	for (var i = 0; i < 10; i ++)
	{
		var x = (Math.random() * 4) - 2;
		var y = (Math.random() * 4) - 2;
		// カメラをその位置の上空に移動させます
		TerraDroneViewer.camera2.position.set(x, y, 5);
		// 真下に向けます。
		TerraDroneViewer.camera2.lookAt(new THREE.Vector3(0, 0, 0));

		raycaster.setFromCamera( mouse, TerraDroneViewer.camera2 );
		// オブジェクトの取得
		var intersects = raycaster.intersectObjects( TerraDroneViewer.maps.children );

		if (intersects.length != 0)
		{
			var z = intersects[0].point.z * 10;
			z = z.toFixed(10);
			console.log("Intersect Z = " + z);
		}
	}
*/
};

///////////////////////////////////////////////////////////////////////////////
// 差分の視覚化
TerraDroneViewer.func.showDiffMesh = function(array)
{
	var geometry = new THREE.Geometry();
//	var material = new THREE.MeshLambertMaterial({color: 0x0000ff, transparent: true, opacity: 0.5});
	var material = new THREE.MeshBasicMaterial({color: 0x0000ff, transparent: true, opacity: 0.5});
	var under_material = new THREE.MeshBasicMaterial({color: 0xff0000, transparent: true, opacity: 0.5});

/*
	for (var i = 0; i < 20; i ++)
	{
		// 今はダミーで生成
		var x = (Math.random() * 4) - 2;
		var y = (Math.random() * 4) - 2;
		var z = TerraDroneViewer.func.getRaycastZpos(x, y);
		if (z == null)
		{
			console.log("処理を省きます");
			continue;
		}
		var box = new THREE.BoxGeometry(
							0.2,
							0.2,
							2);
		var mesh = new THREE.Mesh(box, material);
		mesh.position.set(
			x,
			y,
			z + 8);		// 浮かせます
		// 合成します
		geometry.mergeMesh(mesh);
	}
*/

	// 増加分
	for (var i = 0; i < 200; i ++)
	{
		// 今はダミーで生成
		var x = (Math.random() * 4) - 2;
		var y = (Math.random() * 4) - 2;
		var height = (Math.random()) + 0.5;
		var z = (height / 2) -4 -2;		// この値をZの最高値にすること
		console.log(z);
		var box = new THREE.BoxGeometry(
							TerraDroneViewer.analyze.diff_width,
							TerraDroneViewer.analyze.diff_height,
							height);
		var mesh = new THREE.Mesh(box, material);
		mesh.position.set(
			x,
			y,
			z);		// 浮かせます
		// 合成します
		geometry.mergeMesh(mesh);
	}
	var mesh = new THREE.Mesh(geometry, material);
	mesh.position.set(0, 0, 2);
	TerraDroneViewer.scene.add(mesh);

	// 減少分
	var geometry = new THREE.Geometry();
	for (var i = 0; i < 100; i ++)
	{
		// 今はダミーで生成
		var x = (Math.random() * 2) - 2;
		var y = (Math.random() * 2) - 2;
		var height = (Math.random()) + 0.2;
//		var z = (Math.random() * -2) - 2;
		var z = (height / 2) - 2 -2;
		var box = new THREE.BoxGeometry(
							TerraDroneViewer.analyze.diff_width,
							TerraDroneViewer.analyze.diff_height,
							height);
		var mesh = new THREE.Mesh(box, under_material);
		mesh.position.set(
			x,
			y,
			z);		// 浮かせます
		// 合成します
		geometry.mergeMesh(mesh);
	}
	var mesh = new THREE.Mesh(geometry, under_material);
	mesh.position.set(0, 0, 0);
	TerraDroneViewer.scene.add(mesh);

};

///////////////////////////////////////////////////////////////////////////////
// 領域選択モードへの切替
TerraDroneViewer.func.startAnalyzeVolumeMode = function()
{
	// モード判定
	switch(TerraDroneViewer.system.clickmode)
	{
		case 0:
			break;
		case 1:
			console.error("既に体積計算モードです。");
			return false;
		default:
			console.error("クリックモードが異常です。mode=" + TerraDroneViewer.system.clickmode);
			return false;
	}

	// 領域配列を初期化します。
	TerraDroneViewer.analyze.pos_array = [];
	// 位置選択モードにします。
	TerraDroneViewer.system.clickmode = 1;
};

///////////////////////////////////////////////////////////////////////////////
// 領域追加処理
TerraDroneViewer.func.addPos4Volume = function(latitude, longitude)
{
	if (TerraDroneViewer.func.isMaxVolumeArray())
	{
		console.error("体積管理配列が上限に達しました。max=" + TerraDroneViewer.analyze.pos_max);
		return false;
	}
	TerraDroneViewer.analyze.pos_array.push([latitude, longitude]);
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// 配列が最大かどうか
TerraDroneViewer.func.isMaxVolumeArray = function()
{
	return (TerraDroneViewer.analyze.pos_array.length >= TerraDroneViewer.analyze.pos_max);
};

///////////////////////////////////////////////////////////////////////////////
// 領域選択モードへ完了＆サーバコール
TerraDroneViewer.func.finishAnalyzeVolumeMode = function()
{
	// モード判定
	switch(TerraDroneViewer.system.clickmode)
	{
		case 0:
			console.error("既に体積計算モードです。");
			return false;
		case 1:
			break;
		case 99:
		default:
			console.error("クリックモードが異常です。mode=" + TerraDroneViewer.system.clickmode);
			return false;
	}

	// 抑止モードにします
	TerraDroneViewer.system.clickmode = 99;

	// 配列長を調べます。
	if (TerraDroneViewer.analyze.pos_array.length === 0)
	{
		console.error("位置情報の配列が空っぽです。");
		return false;
	}

	// 配列を文字列に変換します。
	var latlng_str = TerraDroneViewer.analyze.pos_array.toString();

	// 体積計算のURLを生成します。
	var url = TerraDroneViewer.func.createCalcVolumeUrl(TerraDroneViewer.params.project);

	// TODO:
	// サーバへ送信
	// TODO:
	// GETでフォームのIDパスを送信する手法はセキュリティリスクとなる
	var form = new FormData();
	form.append("email", "drone@gmail.com");
	form.append("password", "123123123");

	// TODO:
	// GETでBASIC認証を送信する設計思想はセキュリティリスクとなる
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": url,
		"method": "GET",
		"headers":{
			"authorization": "Basic YWRtaW46YWRtaW4=",
			// この２つはベトナムが提示したサンプルに存在していたが、外さないとjsからはコールできない
//			"cache-control": "no-cache",
//			"postman-token": "66331e13-361d-c4c2-b20f-6376c8e0ff2b"
		},
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form
	};


};

///////////////////////////////////////////////////////////////////////////////
// キャンセル
TerraDroneViewer.func.cancelAnalyzeVolumeMode = function()
{
	// クリックモードを高度計測に変更
	TerraDroneViewer.system.clickmode = 0;

	// 領域配列を初期化します。
	TerraDroneViewer.analyze.pos_array = [];
	// 高度選択モードにします。
	TerraDroneViewer.system.clickmode = 0;
};

