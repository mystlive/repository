///////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// dummy.js
// 実験用ダミー情報定義
////////////////////////////////////////////////////////////////////////////////

TerraDroneViewer.func.initDummy = function()
{
	TerraDroneViewer.dummy = {};
	// ■断面図用データ
	// 元にした情報
	// lat: 15.9988 - 15.9972 （範囲：0.016）
	// lng: 108.248 - 108.246（範囲：0.002)
	TerraDroneViewer.dummy.lines =
	[
		[-4, 3.6, -4.4],
		[0, 0, -4.5],
		[2, 3, -4.47], 
		[2.3, 2.6, -4.72],
		[2.7, 1.3, -4.5],
		[6.0, -5.5, -4.6],
	];
/*

		[15.9976, 108.24602 ],
		[15.9979, 108.24659 ],
		[15.9983, 108.247 ],
*/

	TerraDroneViewer.dummy.volumes = [];
	for (var i = 0; i < 200; i ++)
	{
		// faster code
		TerraDroneViewer.dummy.volumes[i] = 
			[
				(Math.random() * 4) - 2,
				(Math.random() * 4) - 2,
				(Math.random() * -2) - 2
			];
	}

};



