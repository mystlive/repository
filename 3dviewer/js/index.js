///////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// index.js
// メイン処理
// 0.98
// UTF-8/CR+LF/TAB:4
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// init WebGL
TerraDroneViewer.func.init = function()
{
	TerraDroneViewer.container = document.createElement('div');
	document.body.appendChild(TerraDroneViewer.container);

	// メインカメラ
	TerraDroneViewer.camera =
		new THREE.PerspectiveCamera(
			50,
			window.innerWidth / window.innerHeight,
			0.2,
			60);
	TerraDroneViewer.camera.position.set(0, -5, 5);
	// Y-upをZ-upにします
	TerraDroneViewer.camera.up.set( 0, 0, 1 );
/*
	// ヒットテスト専用カメラ
	TerraDroneViewer.camera2 =
		new THREE.PerspectiveCamera(
			50,
			window.innerWidth / window.innerHeight,
			0.2,
			60);
	// Z-upにします。
	TerraDroneViewer.camera2.up.set( 0, 0, 1 );
	// 初期はゼロ点の上空に配置します。
	TerraDroneViewer.camera2.position.set(0, 0, 5);
*/
	// シーンを生成します。
	TerraDroneViewer.scene = new THREE.Scene();
	TerraDroneViewer.scene.background = new THREE.Color( 0x202031 );

	// シーンの下にマップをぶら下げる親を生成します。
	// マウスクリック判定用のマップはこの下に並べます。
	TerraDroneViewer.maps = new THREE.Object3D();
	TerraDroneViewer.scene.add(TerraDroneViewer.maps);

	// Create renderer
	TerraDroneViewer.renderer = new THREE.WebGLRenderer();
	TerraDroneViewer.renderer.setClearColor(0x202031, 1);
	TerraDroneViewer.renderer.setPixelRatio(window.devicePixelRatio);
	TerraDroneViewer.renderer.shadowMapEnabled = true;
	TerraDroneViewer.renderer.setSize(window.innerWidth, window.innerHeight);

	// 全体を通して使用するスケール
	TerraDroneViewer.system.scale = 0.1;

	// Terrain mesh Material
	TerraDroneViewer.materials[0] =
		new THREE.MeshBasicMaterial({
			color: 0xffffff,
			vertexColors: THREE.VertexColors,
		});
	// Vertex Material
	TerraDroneViewer.materials[1] = 
		new THREE.PointCloudMaterial({
			size: 0.13,
			vertexColors: THREE.VertexColors,
			alphaTest: 0.5,
			transparent: true
		});
	// Shader
	var neuronShader = TerraDroneViewer.cloudShader;
	// TODO: async check
	neuronShader.uniforms.map.value = THREE.ImageUtils.loadTexture('point.png');
	// Point Cloud Material
	TerraDroneViewer.materials[2] =
		new THREE.ShaderMaterial({
			uniforms: neuronShader.uniforms,
			attributes: neuronShader.attributes,
			vertexShader: neuronShader.vs,
			fragmentShader: neuronShader.fs,
			vertexColors: THREE.VertexColors,
			transparent: false
		});

	////////////////////////////////////////////////////////////////////////////
	// event

	// Resize Event
	window.onresize = TerraDroneViewer.func.onWindowResize;
	TerraDroneViewer.container.appendChild(TerraDroneViewer.renderer.domElement);

	// mouse
	// コントローラー
	TerraDroneViewer.controls = new THREE.OrbitControls(TerraDroneViewer.camera);

	// マウス移動イベント
	// CPU負荷軽減のために移動ではなくアップ時に変更
	window.addEventListener("mouseup", TerraDroneViewer.func.onMouseUp, false);

};

///////////////////////////////////////////////////////////////////////////////
// Eventとボックス
// 未使用
TerraDroneViewer.func.addHandler = function()
{
	// button
	$("#camera_status").show();

	// カメラリセット
	$(".reset").click(function(){
		TerraDroneViewer.controls.reset();
	});

	// Bounding box
	var box = new THREE.BoxHelper( TerraDroneViewer.mesh );
	TerraDroneViewer.scene.add( box );

	var x = TerraDroneViewer.mesh.geometry.boundingBox.max.x - TerraDroneViewer.mesh.geometry.boundingBox.min.x;
	var y = TerraDroneViewer.mesh.geometry.boundingBox.max.y - TerraDroneViewer.mesh.geometry.boundingBox.min.y;
	var z = TerraDroneViewer.mesh.geometry.boundingBox.max.z - TerraDroneViewer.mesh.geometry.boundingBox.min.z;

	TerraDroneViewer.mesh_box  =
		new THREE.Mesh(
			new THREE.BoxGeometry(
				x * TerraDroneViewer.system.scale,
				y * TerraDroneViewer.system.scale,
				z * TerraDroneViewer.system.scale),
			new THREE.MeshLambertMaterial({color: 0xffffff, transparent: true, opacity:0.3})
			);
	TerraDroneViewer.mesh_box.position.set(0, 0, 0);
	TerraDroneViewer.mesh_box.visible = false;
	TerraDroneViewer.scene.add( TerraDroneViewer.mesh_box );

	$("a.box").click(function(){
		if (TerraDroneViewer.mesh_box.visible) {
			TerraDroneViewer.mesh_box.visible = false;
		}
		else {
			TerraDroneViewer.mesh_box.visible = true;
		}
	});
};

///////////////////////////////////////////////////////////////////////////////
// メッシュのオンオフ切替機能
TerraDroneViewer.func.addVisibleHandler = function() {

	$("#visible").show();

	$("#ply1_mesh").change(function(){
		TerraDroneViewer.mesh.visible = this.checked;
	});
/*`
	$("#ply1_point").change(function(){
		TerraDroneViewer.mesh2.visible = this.checked;
	});
*/
	if (TerraDroneViewer.system.viewMode === 1)
	{
		$("#ply2_mesh").hide();
	}
	else
	{
		$("#ply2_mesh").change(function(){
			TerraDroneViewer.mesh2.visible = this.checked;
		});
	}
/*
	$("#ply2_point").change(function(){
		TerraDroneViewer.mesh4.visible = this.checked;
	});
*/
};

///////////////////////////////////////////////////////////////////////////////
// plyプログレスイベント
TerraDroneViewer.func.onProgress = function(e)
{
	var elem = $("#myBar");
	var percent = $("#percent");
	var per = parseInt((e.loaded / e.total) * 100);
	elem.width(per + "%");
	// 詳細を表示
	percent.html(
		"3D Models part of " + 
		TerraDroneViewer.sequence.current + "/" + TerraDroneViewer.sequence.max + " : " + 
		TerraDroneViewer.dates[TerraDroneViewer.loaded_count] + " " + per + "% Loaded.");
};

///////////////////////////////////////////////////////////////////////////////
// plyロードエラーイベント
TerraDroneViewer.func.onError = function(e)
{
	console.error("plyロードに失敗しました。" + e);
	alert(e);
}

///////////////////////////////////////////////////////////////////////////////
// 全てのロードと初期化の完了・ユーザオペレーション開始
TerraDroneViewer.func.onReady = function()
{

	// delete event
	TerraDroneViewer.loader.removeEventListener('load', TerraDroneViewer.func.onFinishPly, false);
	TerraDroneViewer.loader.removeEventListener('progress', TerraDroneViewer.func.onProgress, false);
	TerraDroneViewer.loader.removeEventListener('error', TerraDroneViewer.func.onError, false);

	// delete func
	TerraDroneViewer.func.onProgress = null;
	TerraDroneViewer.func.onFinishPly = null;
	TerraDroneViewer.func.onError = null;

	// delete Loader
	delete TerraDroneViewer.loader;
	TerraDroneViewer.loader = null;

	// プログレスを消します。
	var elem = $("#myBar");
	elem.hide();
	var percent = $("#percent");
	percent.hide();

	// 表示切替機能
	TerraDroneViewer.func.addVisibleHandler();

	////////////////////////////////////////////////////////////////////////
	// ロード完了後の追加処理

	// 下準備となる解析基盤情報計算
	TerraDroneViewer.func.standby4ConvertPosition();


	// 線引きデモ
	TerraDroneViewer.func.showSectionalLine();
	// 差分デモ
	TerraDroneViewer.func.showDiffMesh();

};

///////////////////////////////////////////////////////////////////////////////
// plyロード完了イベント
TerraDroneViewer.func.onFinishPly = function(e)
{
	console.log("load finihed");

	// ロードカウンタで処理を分けます。
	if (TerraDroneViewer.loaded_count == 0)
	{
		// geometry
		TerraDroneViewer.geometry = e.content;
		e = null;

		// テラインメッシュ
		TerraDroneViewer.mesh =
			new THREE.Mesh(
				TerraDroneViewer.geometry,
				TerraDroneViewer.materials[0]);
		TerraDroneViewer.mesh.scale.set(
									TerraDroneViewer.system.scale,
									TerraDroneViewer.system.scale,
									TerraDroneViewer.system.scale);

		// シーンに追加します
		TerraDroneViewer.maps.add(TerraDroneViewer.mesh);

		// ローダからplyのxyz最大最小情報を抜き出します。
		TerraDroneViewer.params.source.area.x_min = TerraDroneViewer.loader.min[0];
		TerraDroneViewer.params.source.area.x_max = TerraDroneViewer.loader.max[0];
		TerraDroneViewer.params.source.area.y_min = TerraDroneViewer.loader.min[1];
		TerraDroneViewer.params.source.area.y_max = TerraDroneViewer.loader.max[1];
		TerraDroneViewer.params.source.area.z_min = TerraDroneViewer.loader.min[2];
		TerraDroneViewer.params.source.area.z_max = TerraDroneViewer.loader.max[2];

		// 読み込みカウント
		TerraDroneViewer.loaded_count ++;
		TerraDroneViewer.sequence.current ++;

		///////////////////////////////////////////////////////////////////////
		// １つ目の生成が終わった時点で判定を入れます。
		if (TerraDroneViewer.system.viewMode === 1)
		{
			// 全てのロードが完了したら後始末をします。
			TerraDroneViewer.func.onReady();

			return;
		}

		///////////////////////////////////////////////////////////////////////
		// ply2つ目

		// plyのURLを生成(２つ目)
		var url = 
			TerraDroneViewer.func.createPlyUrl(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.target.date);
		// 次のファイルをロード
		TerraDroneViewer.loader.load(url);
	}
	else
	{
		// geometry
		TerraDroneViewer.geometry2 = e.content;
		e = null;

		// テラインメッシュ
		TerraDroneViewer.mesh2 =
			new THREE.Mesh(
				TerraDroneViewer.geometry2,
				TerraDroneViewer.materials[0]);
		TerraDroneViewer.mesh2.scale.set(0.1, 0.1, 0.1);

		// シーンに追加します
		TerraDroneViewer.maps.add(TerraDroneViewer.mesh2);

		// ローダからplyのxyz最大最小情報を抜き出します。
		TerraDroneViewer.params.target.area.x_min = TerraDroneViewer.loader.min[0];
		TerraDroneViewer.params.target.area.x_max = TerraDroneViewer.loader.max[0];
		TerraDroneViewer.params.target.area.y_min = TerraDroneViewer.loader.min[1];
		TerraDroneViewer.params.target.area.y_max = TerraDroneViewer.loader.max[1];
		TerraDroneViewer.params.target.area.z_min = TerraDroneViewer.loader.min[2];
		TerraDroneViewer.params.target.area.z_max = TerraDroneViewer.loader.max[2];

		///////////////////////////////////////////////////////////////////////
		// 全てのロードが完了したら後始末をします。
		TerraDroneViewer.func.onReady();
	}

	console.log("ply draw finihed");
};

///////////////////////////////////////////////////////////////////////////////
// plyロード
TerraDroneViewer.func.loadPly = function(filename)
{
	// シーケンスを加算します
	TerraDroneViewer.sequence.current ++;

	if (TerraDroneViewer.loader == null)
	{
		// PLY file
		TerraDroneViewer.loader = new threeply();
		TerraDroneViewer.loader.addEventListener('progress', TerraDroneViewer.func.onProgress);
		TerraDroneViewer.loader.addEventListener('error', TerraDroneViewer.func.onError);
		TerraDroneViewer.loader.addEventListener('load', TerraDroneViewer.func.onFinishPly);
	}

	// Load
	TerraDroneViewer.loader.load(filename);
};

///////////////////////////////////////////////////////////////////////////////
// onMouseUpイベント派生
// Z位置（高度）測定
TerraDroneViewer.func.analyzeZpos = function(e)
{
	var rect = e.target.getBoundingClientRect();
	// マウス位置(2D)
	var mouseX = e.clientX - rect.left;
	var mouseY = e.clientY - rect.top;

	// マウス位置(3D)
	SCREEN_WIDTH = window.innerWidth;
	SCREEN_HEIGHT= window.innerHeight;
	var mouse = new THREE.Vector2();
	mouse.x = (mouseX/SCREEN_WIDTH) *2 - 1;

	// Raycasterインスタンス作成
	if (TerraDroneViewer.raycaster == null)
	{
		TerraDroneViewer.raycaster = new THREE.Raycaster();
	}
	// 取得したX、Y座標でrayの位置を更新
	TerraDroneViewer.raycaster.setFromCamera( mouse, TerraDroneViewer.camera );

	// オブジェクトの取得
	var intersects = TerraDroneViewer.raycaster.intersectObjects( TerraDroneViewer.maps.children );

	if (intersects.length != 0)
	{
		var z = intersects[0].point.z * 10;
		z = z.toFixed(10);
		$("#sea_level").html(z);
	}
};

///////////////////////////////////////////////////////////////////////////////
// onMouseUpイベント派生
// 位置の追加
TerraDroneViewer.func.appendVolumePos = function(e)
{
	
};

///////////////////////////////////////////////////////////////////////////////
// マウスアップイベント
TerraDroneViewer.func.onMouseUp = function(e)
{
/*
	var rect = e.target.getBoundingClientRect();
	// マウス位置(2D)
	var mouseX = e.clientX - rect.left;
	var mouseY = e.clientY - rect.top;

	// マウス位置(3D)
	SCREEN_WIDTH = window.innerWidth;
	SCREEN_HEIGHT= window.innerHeight;
	var mouse = new THREE.Vector2();
	mouse.x = (mouseX/SCREEN_WIDTH) *2 - 1;

	// Raycasterインスタンス作成
	if (TerraDroneViewer.raycaster == null)
	{
		TerraDroneViewer.raycaster = new THREE.Raycaster();
	}
	// 取得したX、Y座標でrayの位置を更新
	TerraDroneViewer.raycaster.setFromCamera( mouse, TerraDroneViewer.camera );

	// オブジェクトの取得
	var intersects = TerraDroneViewer.raycaster.intersectObjects( TerraDroneViewer.maps.children );

	if (intersects.length != 0)
	{
		var z = intersects[0].point.z * 10;
		z = z.toFixed(10);
		$("#sea_level").html(z);
	}
*/
	// モードで処理分けします
	switch(TerraDroneViewer.system.clickmode)
	{
		case 0:
			TerraDroneViewer.func.analyzeZpos(e);
			break;

		case 1:
			TerraDroneViewer.func.appendVolumePos(2);
			break;

		default:
			console.error("クリックモードが異常です。val=" + TerraDroneViewer.system.clickmode);
	}
};

///////////////////////////////////////////////////////////////////////////////
// マウス移動イベント
// 現時点では重いため未使用
TerraDroneViewer.func.onMouseMove = function(e)
{
	var rect = e.target.getBoundingClientRect();
	// マウス位置(2D)
	var mouseX = e.clientX - rect.left;
	var mouseY = e.clientY - rect.top;

	// マウス位置(3D)
	SCREEN_WIDTH = window.innerWidth;
	SCREEN_HEIGHT= window.innerHeight;
	mouseX = (mouseX/SCREEN_WIDTH) *2 - 1;
	mouseY =-(mouseY/SCREEN_HEIGHT)*2 + 1;

	// マウスベクトル
	var pos = new THREE.Vector3(mouseX, mouseY, 1);
	// pos はスクリーン座標系なので, オブジェクトの座標系に変換
	pos.unproject(TerraDroneViewer.camera);

	var ray = 
		new THREE.Raycaster(
			TerraDroneViewer.camera.position,
			pos.sub(TerraDroneViewer.camera.position).normalize());
	// 交差判定
	var obj = ray.intersectObjects([TerraDroneViewer.mesh]);

	if (obj.length > 0) {
		console.log("hit count=" + obj.length);
	}
};

///////////////////////////////////////////////////////////////////////////////
// ウィンドウサイズ変化
TerraDroneViewer.func.onWindowResize = function()
{
	TerraDroneViewer.camera.aspect = window.innerWidth / window.innerHeight;
	TerraDroneViewer.camera.updateProjectionMatrix();
	TerraDroneViewer.renderer.setSize(window.innerWidth, window.innerHeight);
};

///////////////////////////////////////////////////////////////////////////////
// animate
TerraDroneViewer.func.animate = function()
{
	TerraDroneViewer.controls.update();
	TerraDroneViewer.stats.update();
	TerraDroneViewer.renderer.render(TerraDroneViewer.scene, TerraDroneViewer.camera);
	requestAnimationFrame(TerraDroneViewer.func.animate);
};

///////////////////////////////////////////////////////////////////////////////
// 初期コール
$(function(){
	// ボタンなどを不可視にします
	$("#camera_status").hide();
	$("#plyname").hide();
	$("#visible").hide();

	// 共通で使用する定数等の初期化
	TerraDroneViewer.func.define();
	// Three.jsの初期化
	TerraDroneViewer.func.init();

	// ヘルパを先に描画します
	TerraDroneViewer.helper = new ThreeJsHelper();
	TerraDroneViewer.helper.grid();
	TerraDroneViewer.helper.axis();
	TerraDroneViewer.helper.stats();

	// 操作性の点から、ここで既にアニメーションを開始しておきます。
	TerraDroneViewer.func.animate();

	// HTMLに付けられた引数をパースします。
	if (!TerraDroneViewer.func.parseQuery())
	{
		alert("htmlコール時に引数を指定して下さい。\n処理を中断します。");
		return;
	}

	// 体積・線分用定数の初期化
	TerraDroneViewer.func.geoAnalyzeInit();

	// 線引きデモ
//	TerraDroneViewer.func.showSectionalLine();
//	TerraDroneViewer.func.showDiffMesh();

	TerraDroneViewer.light = new THREE.DirectionalLight(0xffffff, 0.9);
	TerraDroneViewer.light.position.set(-30, -30, 30);
	TerraDroneViewer.scene.add(TerraDroneViewer.light);

	///////////////////////////////////////////////////////////////
	// 固定で良いとの事なのでKMLからロード開始
	TerraDroneViewer.func.loadKml();
});
