////////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// coomon.js
// 共通定義
// ver 0.8
// UTF-8/CR+LF/TAB:4
////////////////////////////////////////////////////////////////////////////////

var TerraDroneViewer = TerraDroneViewer||{};
TerraDroneViewer.func = {};
TerraDroneViewer.loader = {};
TerraDroneViewer.handler = {};


///////////////////////////////////////////////////////////////////////////////
// 変数群の定義
TerraDroneViewer.func.define = function() {

	// Three.js
	TerraDroneViewer.container = {};
	TerraDroneViewer.camera = {};
	TerraDroneViewer.cameraTarget = {};
	TerraDroneViewer.scene = {};
	TerraDroneViewer.renderer = {};
	TerraDroneViewer.materials = [];
	TerraDroneViewer.mesh = {};
	TerraDroneViewer.controls = {};
	TerraDroneViewer.stats = {};
	TerraDroneViewer.loader = null;
	TerraDroneViewer.geometry = {};
	TerraDroneViewer.cloudShader = {
		attributes: {
			alpha: {
				type: 'f',
				value: []
			}
		},
		uniforms: {
			scale: {
				type: 'f',
				value: 250.0
			},
			size: {
				type: 'f',
				value: 0.07				// 粒度
			},
			diffuse: {
				type: 'c',
				value: new THREE.Color(1, 1, 1)
			},
			map: {
				type: 't',
				value: null
			}
		},
		fs:[
			'uniform sampler2D map;',
			'uniform vec3 diffuse;',
			'varying vec3 vColor;',
			'varying float vAlpha;',
			'void main(){',
			'    vec2 uv = vec2( gl_PointCoord.x, 1.0 - gl_PointCoord.y );',
			'    vec4 tex = texture2D( map, uv );',
			'    gl_FragColor = vec4(diffuse*vColor*tex.xyz, vAlpha*tex.a);',
			'}'
		].join('\n'),
		vs:[	  
			'attribute float alpha;',
			'uniform float scale;',
			'uniform float size;',
			'varying vec3 vColor;',
			'varying float vAlpha;',
			'void main(){',
			'    vColor.xyz = color.xyz;',
			'    vAlpha = alpha;',
			'    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
			'    gl_PointSize = size * ( scale / length( mvPosition.xyz ) );',
			'    gl_Position = projectionMatrix * mvPosition;',
			'}'
		].join('\n')
	};

	// 共通変数
	TerraDroneViewer.system = {};
	TerraDroneViewer.system.base_z = -5;				// グリッド配置位置
	TerraDroneViewer.system.latlng_digit = 8;			// 緯度経度の分割

	// グリッド全体が左右に伸びるサイズ
	// TODO:ply解析によって変動させる必要があります
	TerraDroneViewer.system.grid_size = 9;
	// グリッド線の間隔
	TerraDroneViewer.system.grid_interval = 1;

	// 表示モード
	// 数値がそのままモデル数です。
	// 1:単体表示
	// 2:同一エリアの日時相違モデルを2つ表示
	TerraDroneViewer.system.viewMode = 1;

	// ファイル読み込みカウント
	TerraDroneViewer.loaded_count = 0;

	// メッシュ・ポイントのどちらを読むのか判断用
	// 0:メッシュ
	// 1:ポイントクラウド
	TerraDroneViewer.rendererMode = 0;

	// デバッグサーバへの向き
	// 0:ステージング
	// 1:プロダクション
	TerraDroneViewer.is_production_server = 0;

	// サーバURL（向きを同梱）
	TerraDroneViewer.base_url = [
		"https://gis-dev.terra-utm.com",
		"https://gis.terra-utm.com",
	];

	// APIディレクトリ
	TerraDroneViewer.api_path = "/api/v1/project";

	// 各種API等
	TerraDroneViewer.url = {};
	// 2017/05/08 sourceが不要となった。また、単体KMLのアドレスが変更
//	TerraDroneViewer.url.kml_single = "/${PROJECT}/map_bound.kml?source=${SOURCE}";
	TerraDroneViewer.url.kml_single = "/${PROJECT}/single_map_bound.kml";
	TerraDroneViewer.url.kml_double = "/${PROJECT}/map_bound.kml?source=${SOURCE}&target=${TARGET}";
	TerraDroneViewer.url.diff_volume = "/${PROJECT}/surface_compare";
	TerraDroneViewer.url.calc_volume = "/${PROJECT}/elevation_array";

	// 2017/05/08 dateが不要となった
//	TerraDroneViewer.url.ply = "/${PROJECT}/${DATE}/3d_model/quarry_simplified_3d_mesh.ply";
	TerraDroneViewer.url.ply = "/${PROJECT}/3d_model/quarry_simplified_3d_mesh.ply";

	// 変換マクロ
	TerraDroneViewer.macro = {};
	TerraDroneViewer.macro.project = "${PROJECT}";
	TerraDroneViewer.macro.date = "${DATE}";
	TerraDroneViewer.macro.source = "${SOURCE}";
	TerraDroneViewer.macro.target = "${TARGET}";

	// HTMLの引数から取得できる変数
	TerraDroneViewer.params = {};
	TerraDroneViewer.params.project = {};
	TerraDroneViewer.params.source = {};
	TerraDroneViewer.params.target = {};
	TerraDroneViewer.params.source.area = {};
	TerraDroneViewer.params.target.area = {};
	TerraDroneViewer.params.source.date;
	TerraDroneViewer.params.target.date;
	TerraDroneViewer.params.project;
	TerraDroneViewer.params.coordinate;

	// HTML引数で指定される変数名
	TerraDroneViewer.params.param_names = [
		"project",
		"source_date",
		"target_date",
		"coordinate"
	];

	// src/dstの日付は実装都合上、配列でも保持します
	TerraDroneViewer.dates = [];

	// ロードシーケンス
	// 起動中に(n/2)の表示を行うための変数です。
	// view_modeによって値が変化します。
	TerraDroneViewer.sequence = {};
	TerraDroneViewer.sequence.max = 2;		// 最大
	TerraDroneViewer.sequence.current = 0;	// 現在値

	// 断面図管理用配列
	// 配列の添字をIDとして管理します。
	TerraDroneViewer.diff = {};
	TerraDroneViewer.diff.point_array = [];

	// 差分管理用配列
	// 配列の添字をIDとして管理します。
	TerraDroneViewer.volume = {};
	TerraDroneViewer.volume.point_array = [];

	// 解析情報
	TerraDroneViewer.analyze = {};

	// TODO:
	// 北緯南緯東経西経による処理分岐が完全ではない
	// これらのフラグを判断し逆転させる必要がある
	TerraDroneViewer.analyze.is_north = true;
	TerraDroneViewer.analyze.is_east = true;

	// 範囲比較の元計算が完了しているかどうか
	// 条件はKMLのロードplyの描画です。
	TerraDroneViewer.analyze.standby_conv_pos = false;

	/////////////////////////////////////////////////////////////////////////////
	// クリックモード
	// 0: Z位置(高度)計測モード
	// 1: 体積計算モード
	// 99:クリック抑止モード
	TerraDroneViewer.system.clickmode = 0;

	// 体積計測クリック位置を保存する配列
	TerraDroneViewer.analyze.pos_array = [];
	// クリック範囲の最大数（ArcPyの上限が不明なため）
	TerraDroneViewer.analyze.pos_max = 16;

};
