////////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// projectAnalyze.js
// プロジェクト解析処理
// ver 0.95
// UTF-8/CR+LF/TAB:4
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// 配置ドメインの生成
TerraDroneViewer.func.createServerUrl = function()
{
	// TODO:サーバ向きフラグ値の正当性
	return TerraDroneViewer.base_url[TerraDroneViewer.is_production_server];
};

////////////////////////////////////////////////////////////////////////////////
// KMLのアドレス取得（１個版）
// 先々の仕様が不明なので構造化しておきます
// @param	project		プロジェクトID
// @param	src			ソースの日付YYYY-MM-DD
TerraDroneViewer.func.createKmlUrlSingle = function(project, src)
{
	// 単体用KML文字列の置き換えを行います。
	var url = TerraDroneViewer.url.kml_single.replace(TerraDroneViewer.macro.project, project);
	// 2017/05/08 sourceが不要となった
//	url = url.replace(TerraDroneViewer.macro.source, src);

	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// KMLのアドレス取得（２個版）
// 先々の仕様が不明なので構造化しておきます
// @param	project		プロジェクトID
// @param	src			ソースの日付YYYY-MM-DD
// @param	dst			デスティネーションの日付YYYY-MM-DD
TerraDroneViewer.func.createKmlUrlDouble = function(project, src, dst)
{
	// ２個用KML文字列の置き換えを行います。
	var url = TerraDroneViewer.url.kml_double.replace(TerraDroneViewer.macro.project, project);
	url = url.replace(TerraDroneViewer.macro.source, src);
	url = url.replace(TerraDroneViewer.macro.target, dst);

	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// 差分jsonのアドレス取得
// @param	project		プロジェクトID
TerraDroneViewer.func.createDiffVolumeUrl = function(project)
{
	var url = TerraDroneViewer.url.diff_volume.replace(TerraDroneViewer.macro.project, project);
	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// plyのアドレス取得
// @param	project		プロジェクトID
// @param	date		日付YYYY-MM-DD
TerraDroneViewer.func.createPlyUrl = function(project, date)
{
	var url = TerraDroneViewer.url.ply.replace(TerraDroneViewer.macro.project, project);
	// 2017/05/08 sourceが不要となった。
//	url = url.replace(TerraDroneViewer.macro.date, date);
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// KMLのロードとパース
TerraDroneViewer.func.loadKml = function()
{
	// ビューモードで処理を分けます。
	switch(TerraDroneViewer.system.viewMode)
	{
		// 単体表示
		case 1:
			// モード判定成功
			// APIコールに時間が掛かり過ぎているため、スピナーを表示します
			TerraDroneViewer.helper.showSpinner();

			// 単体のKMLをロードします
			TerraDroneViewer.func.loadKmlSingle();
			break;

		// ２個表示
		case 2:
			// モード判定成功
			// APIコールに時間が掛かり過ぎているため、スピナーを表示します
			TerraDroneViewer.helper.showSpinner();

			// ２個分のKMLをロードします
			TerraDroneViewer.func.loadKmlDouble();
			break;

		default:
			console.error("単体表示モードで差分情報取得はできません");
			return false;
	}

};

////////////////////////////////////////////////////////////////////////////////
// 単体KMLロードサブ関数
TerraDroneViewer.func.loadKmlSingle = function()
{
	// アドレスを動的生成します
	var url = TerraDroneViewer.func.createKmlUrlSingle(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.source.date);

	// TODO:
	// GETでフォームのIDパスを送信する手法はセキュリティリスクとなる
	var form = new FormData();
	form.append("email", "drone@gmail.com");
	form.append("password", "123123123");

	// TODO:
	// GETでBASIC認証を送信する設計思想はセキュリティリスクとなる
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": url,
		"method": "GET",
		"headers":{
			"authorization": "Basic YWRtaW46YWRtaW4=",
			// この２つはベトナムが提示したサンプルに存在していたが、外さないとjsからはコールできない
//			"cache-control": "no-cache",
//			"postman-token": "66331e13-361d-c4c2-b20f-6376c8e0ff2b"
		},
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form,
		// この２つはベトナムが提示したサンプルで足りていない
		"contentType":'text/xml',
		"dataType": 'xml',
	};

/*
	$.ajax({
		type:'GET',
		contentType:'text/xml',
		url:url,
		dataType: 'xml',
		headers:{
			Authorization: 'Basic YWRtaW46YWRtaW4=',
			"postman-token": "66331e13-361d-c4c2-b20f-6376c8e0ff2b",
		},
		"data": form
	})
*/
	// kmlをロードします
	$.ajax(settings)
	.done(function (response, textStatus, jqXHR) {
		if (textStatus === "err") {
			console.error("緯度経度のロードに失敗しました。");
			alert("緯度経度のロードに失敗しました。処理を中止します。");
			return;
		}
		else {
			console.log(response);
			// XMLをパースしグローバルにそのまま入れ込んでしまいます
			var xmlDoc = $(response);
			var src = xmlDoc.find("Source");
			// src
			TerraDroneViewer.params.source.north = parseFloat(src.find("north").text());
			TerraDroneViewer.params.source.south = parseFloat(src.find("south").text());
			TerraDroneViewer.params.source.east = parseFloat(src.find("east").text());
			TerraDroneViewer.params.source.west = parseFloat(src.find("west").text());

			xmlDoc = null;

			// ヘルパを表示します。
			$(".i2Style.zpos").show();
			TerraDroneViewer.helper.latilong();

			////////////////////////////////////////////////////////////////////////
			// KML単体の場合は差分情報のロードは行いません。
			// plyのロードを開始します。
			// スピナーを止めてプログレスバーに切り替えます。
			TerraDroneViewer.helper.hideSpinner();

			// plyのURLを生成します
			var url = 
				TerraDroneViewer.func.createPlyUrl(
					TerraDroneViewer.params.project,
					TerraDroneViewer.params.source.date);

			////////////////////////////////////////////////////////////////////////
			// ply１つ目のロード開始します
			TerraDroneViewer.func.loadPly(url);

		}
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		console.error(textStatus);
		alert("緯度経度のロードに失敗しました。処理を中止します。");
	});

};

////////////////////////////////////////////////////////////////////////////////
// 複数KMLロードサブ関数
TerraDroneViewer.func.loadKmlDouble = function()
{
	// アドレスを動的生成します
	var url = TerraDroneViewer.func.createKmlUrlDouble(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.source.date,
				TerraDroneViewer.params.target.date);

	// jsonをロードします
	$.ajax({
		type:'GET',
		contentType:'text/xml',
		url:url,
		dataType: 'xml',
		headers:{
			Authorization: 'Basic YWRtaW46YWRtaW4=',
		}
	})
	.done(function (response, textStatus, jqXHR) {
		if (textStatus === "err") {
			console.error("緯度経度のロードに失敗しました。");
			alert("緯度経度のロードに失敗しました。処理を中止します。");
			return;
		}
		else {
			console.log(response);
			// XMLをパースしグローバルにそのまま入れ込んでしまいます
			var xmlDoc = $(response);
			var src = xmlDoc.find("Source");
			var dst = xmlDoc.find("Target");

			// src
			TerraDroneViewer.params.source.north = parseFloat(src.find("north").text());
			TerraDroneViewer.params.source.south = parseFloat(src.find("south").text());
			TerraDroneViewer.params.source.east = parseFloat(src.find("east").text());
			TerraDroneViewer.params.source.west = parseFloat(src.find("west").text());

			// dst
			TerraDroneViewer.params.target.north = parseFloat(dst.find("north").text());
			TerraDroneViewer.params.target.south = parseFloat(dst.find("south").text());
			TerraDroneViewer.params.target.east = parseFloat(dst.find("east").text());
			TerraDroneViewer.params.target.west = parseFloat(dst.find("west").text());

			xmlDoc = null;

			$(".i2Style.zpos").show();
			TerraDroneViewer.helper.latilong();

			///////////////////////////////////////////////////////////////////
			// 体積差分jsonのコールにつなげます。
			TerraDroneViewer.func.VolumeDiff();
		}
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		alert("緯度経度のロードに失敗しました。処理を中止します。");
	});

};

////////////////////////////////////////////////////////////////////////////////
// 比較差分の取得
TerraDroneViewer.func.VolumeDiff = function()
{
	// ビューモードを確認します。
	if (TerraDroneViewer.system.viewMode !== 2)
	{
		console.error("単体表示モードで差分情報取得はできません");
		return false;
	}

	// アドレスを動的生成します
	var url = TerraDroneViewer.func.createDiffVolumeUrl(TerraDroneViewer.params.project);

/*

	///////////////////////////////////////////////////////////////////////////
	// 要CHECK:
	// stringifyがお手本のはずだが、良くある方法ではサーバが受け付けてくれない
	// 空白が%20に変換されると失敗？

	var body = 
		JSON.stringify({

			// 正しく戻るbodyの指定方法
			"source_date" : "2017-02-28",
			"target_date" : "2017-03-01",
			"coordinate" : [[15.9985806, 108.2466817], [15.9984237, 108.2468403], [15.9984262, 108.2465959]]

			// 失敗するbodyの指定方法
			"source_date" : TerraDroneViewer.params.source_date,
			"target_date" : TerraDroneViewer.params.target_date,
			"coordinate" : TerraDroneViewer.params.coordinate
		});

	// 
	///////////////////////////////////////////////////////////////////////////

*/

	// この手動指定方法でないと通らない。
	// サーバサイドでのパース処理か正規表現に問題があるものと思われる
	var body = "{" + 
			"\"source_date\":\"" + TerraDroneViewer.params.source.date +
			"\",\"target_date\":\"" + TerraDroneViewer.params.target.date +
			"\",\"coordinate\":" + TerraDroneViewer.params.coordinate +
			"}";

	// jsonをロードします
	$.ajax({
		type:'POST',
		contentType:'application/json',
		url:url,
		dataType: 'json',
		headers:{
			Authorization: 'Basic YWRtaW46YWRtaW4='
		},
		data: body
	})
	.done(function (response, textStatus, jqXHR) {
		console.log(jqXHR.responseText);
		var json = jqXHR.responseJSON;
		if (json.success === false)
		{
			alert("体積差分のロードに失敗しました。処理を中止します。");
			return;
		}

		var diff = json.data.features[0].properties.Volume;
		$(".i2Style.diff").show();
		$("#diff_volume").html(diff);

		////////////////////////////////////////////////////////////////////////
		// ここまで来たらplyのロードを開始します。
		// スピナーを止めてプログレスバーに切り替えます。
		TerraDroneViewer.helper.hideSpinner();

		// plyのURLを生成
		var url = 
			TerraDroneViewer.func.createPlyUrl(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.source.date);

		////////////////////////////////////////////////////////////////////////
		// ply１つ目のロード開始
		TerraDroneViewer.func.loadPly(url);
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		alert("体積差分のロードに失敗しました。処理を中止します。");
	});
};

