////////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// projectAnalyze.js
// プロジェクト解析処理
// ver 0.95
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// 配置ドメインの生成
TerraDroneViewer.func.createServerUrl = function()
{
	// TODO:サーバ向きフラグ値の正当性
	return TerraDroneViewer.base_url[TerraDroneViewer.is_production_server];
};

////////////////////////////////////////////////////////////////////////////////
// KMLのアドレス取得
TerraDroneViewer.func.createKmlUrl = function(project, src, dst)
{
	// 置き換えを行います。
	var url = TerraDroneViewer.url.kml.replace(TerraDroneViewer.macro.project, project);
	url = url.replace(TerraDroneViewer.macro.source, src);
	url = url.replace(TerraDroneViewer.macro.target, dst);

	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// 差分jsonのアドレス取得
TerraDroneViewer.func.createVolumeUrl = function(project)
{
	var url = TerraDroneViewer.url.volume.replace(TerraDroneViewer.macro.project, project);
	// 合成して返します。
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// plyのアドレス取得
TerraDroneViewer.func.createPlyUrl = function(project, date)
{
	var url = TerraDroneViewer.url.ply.replace(TerraDroneViewer.macro.project, project);
	url = url.replace(TerraDroneViewer.macro.date, date);
	return TerraDroneViewer.func.createServerUrl() +
			TerraDroneViewer.api_path +
			url;
};

////////////////////////////////////////////////////////////////////////////////
// KMLのロードとパース
TerraDroneViewer.func.loadKml = function()
{
	// シーケンスを加算します
//	TerraDroneViewer.sequence.current ++;

	// APIコールに時間が掛かり過ぎているため、スピナーを表示します
	TerraDroneViewer.helper.showSpinner();

	// アドレスを動的生成します
	var url = TerraDroneViewer.func.createKmlUrl(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.source.date,
				TerraDroneViewer.params.target.date);

	// jsonをロードします
	$.ajax({
		type:'GET',
		contentType:'text/xml',
		url:url,
		dataType: 'xml',
		headers:{
			Authorization: 'Basic YWRtaW46YWRtaW4='
		}
	})
	.done(function (response, textStatus, jqXHR) {
		if (textStatus === "err") {
			alert("緯度経度のロードに失敗しました。処理を中止します。");
			return;
		}
		else {
			console.log(response);
			// XMLをパースしグローバルにそのまま入れ込んでしまいます
			var xmlDoc = $(response);
			var src = xmlDoc.find("Source");
			var dst = xmlDoc.find("Target");

			// src
			TerraDroneViewer.params.source.north = parseFloat(src.find("north").text());
			TerraDroneViewer.params.source.south = parseFloat(src.find("south").text());
			TerraDroneViewer.params.source.east = parseFloat(src.find("east").text());
			TerraDroneViewer.params.source.west = parseFloat(src.find("west").text());

			// dst
			TerraDroneViewer.params.target.north = parseFloat(dst.find("north").text());
			TerraDroneViewer.params.target.south = parseFloat(dst.find("south").text());
			TerraDroneViewer.params.target.east = parseFloat(dst.find("east").text());
			TerraDroneViewer.params.target.west = parseFloat(dst.find("west").text());

			xmlDoc = null;

			$(".i2Style.zpos").show();
			TerraDroneViewer.helper.latilong();

			///////////////////////////////////////////////////////////////////
			// 体積差分jsonのコール
			TerraDroneViewer.func.VolumeDiff();
		}
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		alert("緯度経度のロードに失敗しました。処理を中止します。");
	});
};

////////////////////////////////////////////////////////////////////////////////
// 比較差分の取得
TerraDroneViewer.func.VolumeDiff = function()
{
	// シーケンスを加算します
//	TerraDroneViewer.sequence.current ++;

	// アドレスを動的生成します
	var url = TerraDroneViewer.func.createVolumeUrl(TerraDroneViewer.params.project);

/*
	// 失敗する処理。stringifyがお手本のはず。
	var body = 
		JSON.stringify({

			// 正しく戻るbodyの指定方法
			"source_date" : "2017-02-28",
			"target_date" : "2017-03-01",
			"coordinate" : [[15.9985806, 108.2466817], [15.9984237, 108.2468403], [15.9984262, 108.2465959]]

			// 失敗するbodyの指定方法
			"source_date" : TerraDroneViewer.params.source_date,
			"target_date" : TerraDroneViewer.params.target_date,
			"coordinate" : TerraDroneViewer.params.coordinate
		});
*/

	// この手動指定方法でないと通らない。
	// サーバサイドでのパース処理か正規表現に問題があるものと思われる
	var body = "{" + 
			"\"source_date\":\"" + TerraDroneViewer.params.source.date +
			"\",\"target_date\":\"" + TerraDroneViewer.params.target.date +
			"\",\"coordinate\":" + TerraDroneViewer.params.coordinate +
			"}";

	// jsonをロードします
	$.ajax({
		type:'POST',
		contentType:'application/json',
		url:url,
		dataType: 'json',
		headers:{
			Authorization: 'Basic YWRtaW46YWRtaW4='
		},
		data: body
	})
	.done(function (response, textStatus, jqXHR) {
		console.log(jqXHR.responseText);
		var json = jqXHR.responseJSON;
		if (json.success === false)
		{
			alert("体積差分のロードに失敗しました。処理を中止します。");
			return;
		}

		var diff = json.data.features[0].properties.Volume;
		$(".i2Style.diff").show();
		$("#diff_volume").html(diff);

		////////////////////////////////////////////////////////////////////////
		// ここまで来たらplyのロードを開始します。
		// スピナーを止めてプログレスバーに切り替えます。
		TerraDroneViewer.helper.hideSpinner();

		// plyのURLを生成
		var url = 
			TerraDroneViewer.func.createPlyUrl(
				TerraDroneViewer.params.project,
				TerraDroneViewer.params.source.date);

		////////////////////////////////////////////////////////////////////////
		// ply１つ目のロード開始
		TerraDroneViewer.func.loadPly(url);
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		alert("体積差分のロードに失敗しました。処理を中止します。");
	});
};

