////////////////////////////////////////////////////////////////////////////////
// TerraDrone用3Dビューア
// THREE.threeply
// 巨大サイズバイナリ型plyファイル専用ローダ
// ver 0.9.7
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
threeply = function() {
	this.propertyNameMapping = {};
	// 最小最大を記憶する変数
	this.min = [0, 0, 0];
	this.minFirst = [true, true, true];
	this.max = [0, 0, 0];
	this.maxFirst = [true, true, true];
};
threeply.prototype = {
	constructor: threeply,
	setPropertyNameMapping: function(mapping) {
		this.propertyNameMapping = mapping;
	},
	load: function(url, callback) {
		var scope = this;
		var req = new XMLHttpRequest();
		req.addEventListener('load', function(e) {
			console.log("step1");

			// データ→文字列変換とパースでメモリがオーバーフローしているため、強制的にバイナリとして扱う
			// e.target.responseの型式はArrayBuffer

			var geometry = scope.parseBinary(e.target.response);
			console.log("step2");
			scope.dispatchEvent({
				type: 'load',
				content: geometry
			});
		}, false);
		req.addEventListener('progress', function(e) {
			scope.dispatchEvent({
				type: 'progress',
				loaded: e.loaded,
				total: e.total
			});
		}, false);
		req.addEventListener('error', function() {
			scope.dispatchEvent({
				type: 'error',
				message: 'Couldn\'t load URL [' + url + ']'
			});
		}, false);
		req.open('GET', url, true);

		// TerraDroneサーバ特有の認証
		// TODO:認証情報の一元化、リテラル廃止
		req.setRequestHeader( 'Content-Type', 'text/xml' );
		req.setRequestHeader('Authorization', 'Basic YWRtaW46YWRtaW4=');

		req.responseType = "arraybuffer";
		req.send(null);
	},
	parseHeader: function(data) {
		var patternHeader = /ply([\s\S]*)end_header\s/;
		var headerText = "";
		var headerLength = 0;
		var result = patternHeader.exec(data);
		if (result !== null) {
			headerText = result[1];
			headerLength = result[0].length;
		}
		var header = {
			comments: [],
			elements: [],
			headerLength: headerLength
		};
		var lines = headerText.split('\n');
		var iElement = undefined;
		var lineType, lineValues;

		function make_ply_element_property(propertValues, propertyNameMapping) {
			var property = {
				type: propertValues[0]
			};
			if (property.type === 'list') {
				property.name = propertValues[3];
				property.countType = propertValues[1];
				property.itemType = propertValues[2];
			} else {
				property.name = propertValues[1];
			}
			if (property.name in propertyNameMapping) {
				property.name = propertyNameMapping[property.name];
			}
			return property;
		}
		var len = lines.length;	// faster 
		for (var i = 0; i < len; i++) {
			var line = lines[i];
			line = line.trim();
			if (line === "") {
				continue;
			}
			lineValues = line.split(/\s+/);
			lineType = lineValues.shift()
			line = lineValues.join(" ")
			switch (lineType) {
				case "format":
					header.format = lineValues[0];
					header.version = lineValues[1];
					break;
				case "comment":
					header.comments.push(line);
					break;
				case "element":
					if (!(iElement === undefined)) {
						header.elements.push(iElement);
					}
					iElement = {};
					iElement.name = lineValues[0];
					iElement.count = parseInt(lineValues[1]);
					iElement.properties = [];
					break;
				case "property":
					iElement.properties.push(make_ply_element_property(lineValues, this.propertyNameMapping));
					break;
				default:
					console.log("unhandled", lineType, lineValues);
			}
		}
		if (!(iElement === undefined)) {
			header.elements.push(iElement);
		}
		return header;
	},
	postProcess: function(geometry) {
		// Terrain Color
		if (geometry.useColor) {
			var len = geometry.faces.length;
			for (var i = 0; i < len; i ++) {
				geometry.faces[i].vertexColors = [
					geometry.colors[geometry.faces[i].a],
					geometry.colors[geometry.faces[i].b],
					geometry.colors[geometry.faces[i].c]
				];
			}
			geometry.elementsNeedUpdate = true;
		}
		geometry.computeBoundingSphere();
		return geometry;
	},
	// 記録した値より小さければ保存します。
	minPos : function(doubt, index) {
		if (this.minFirst[index]) {
			this.minFirst[index] = false;
			this.min[index] = doubt;
			return;
		}
		if (this.min[index] > doubt) {
			this.min[index] = doubt;
		}
	},
	// 記録した値より大きければ保存します。
	maxPos : function(doubt, index) {
		if (this.maxFirst[index]) {
			this.maxFirst[index] = false;
			this.max[index] = doubt;
			return;
		}
		if (doubt > this.max[index]) {
			this.max[index] = doubt;
		}
	},
	minMaxPos : function(doubt, index) {
		this.minPos(doubt, index);
		this.maxPos(doubt, index);
	},
	handleElement: function(geometry, elementName, element) {
		if (elementName === "vertex") {
			// 高さの最大最小値を記録します
			this.minMaxPos(element.x, 0);
			this.minMaxPos(element.y, 1);
			this.minMaxPos(element.z, 2);

			geometry.vertices.push(new THREE.Vector3(element.x, element.y, element.z));
			if ('red' in element && 'green' in element && 'blue' in element) {
				geometry.useColor = true;
				var color = new THREE.Color();
				color.setRGB(element.red / 255.0, element.green / 255.0, element.blue / 255.0);
				geometry.colors.push(color);
			}
		} else if (elementName === "face") {

			var vertex_indices = element.vertex_indices;
			switch(vertex_indices.length)
			{
			case 3:
				geometry.faces.push(
					new THREE.Face3(
						vertex_indices[0],
						vertex_indices[1],
						vertex_indices[2]));
				break;
			case 4:
				geometry.faces.push(
					new THREE.Face3(
						vertex_indices[0],
						vertex_indices[1],
						vertex_indices[3]), 
					new THREE.Face3(
						vertex_indices[1],
						vertex_indices[2],
						vertex_indices[3]));
				break;
			default:
				console.log("faceの長さが異常です");
			}
		}
	},
	readBin: function(dataview, at, type, is_little_endian) {
		switch (type) {
			case 'int8':
			case 'char':
				return [dataview.getInt8(at), 1];
			case 'uint8':
			case 'uchar':
				return [dataview.getUint8(at), 1];
			case 'int16':
			case 'short':
				return [dataview.getInt16(at, is_little_endian), 2];
			case 'uint16':
			case 'ushort':
				return [dataview.getUint16(at, is_little_endian), 2];
			case 'int32':
			case 'int':
				return [dataview.getInt32(at, is_little_endian), 4];
			case 'uint32':
			case 'uint':
				return [dataview.getUint32(at, is_little_endian), 4];
			case 'float32':
			case 'float':
				return [dataview.getFloat32(at, is_little_endian), 4];
			case 'float64':
			case 'double':
				return [dataview.getFloat64(at, is_little_endian), 8];
		}
	},
	readBinElement: function(dataview, at, properties, is_little_endian) {
		var element = Object();
		var result, read = 0;
		var len = properties.length;	// faster
		for (var i = 0; i < len; i++) {
			if (properties[i].type === "list") {
				var list = [];
				result = this.readBin(dataview, at + read, properties[i].countType, is_little_endian);
				var n = result[0];
				read += result[1];
				for (var j = 0; j < n; j++) {
					result = this.readBin(dataview, at + read, properties[i].itemType, is_little_endian);
					list.push(result[0]);
					read += result[1];
				}
				element[properties[i].name] = list;
			} else {
				result = this.readBin(dataview, at + read, properties[i].type, is_little_endian);
				element[properties[i].name] = result[0];
				read += result[1];
			}
		}
		return [element, read];
	},
	parseBinary: function(data) {
		// 引数のdataは丸ごとのデータなので、殆どの場合に対応できると思われるサイズのみ「Uint8」形式でで取り出す
		var array_buffer = new Uint8Array(data, 0, 320);
		var head_doubt_str = '';
		var len = array_buffer.byteLength;
		for (var i = 0; i < len; i ++)
		{
			head_doubt_str += String.fromCharCode(array_buffer[i]); // implicitly assumes little-endian
		}

		// TODO:
		// もしこの中にend headerが無かったら失敗
		if (head_doubt_str.indexOf("end_header") == -1)
		{
			alret("この形式には対応できていません。");
			return null;
		}
		// ヘッダ文字列をパースし詳細を解析
		var header = this.parseHeader(head_doubt_str);
		// 恐らくPix4Dから出力される形式は全てlittleなので固定でも良いかも知れない
		var is_little_endian = (header.format === "binary_little_endian");
		var body = new DataView(data, header.headerLength);
		var result, loc = 0;

		var geometry = new THREE.Geometry();
		var len = header.elements.length;
		for (var i = 0; i < len; i++) {
			var count = header.elements[i].count;
			for (var j = 0; j < count; j++) {
				result = this.readBinElement(body, loc, header.elements[i].properties, is_little_endian);
				loc += result[1];
				var element = result[0];
				this.handleElement(geometry, header.elements[i].name, element);
			}
		}
		header = null;
		return this.postProcess(geometry);
	}
};
THREE.EventDispatcher.prototype.apply(threeply.prototype);
