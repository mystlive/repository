<?php
////////////////////////////////////////////////////////////////////////////////
//
// 3d視覚化ビューア用サンプルコード
//
////////////////////////////////////////////////////////////////////////////////

$result = array(
	"path1" => array(
		"dir" => "http://woohsucat.sakura.ne.jp/Neolab/ply/ply/",
		"ply" => "quarry_simplified_3d_mesh_20170228.ply"
//		"ply" => "woman.ply"
	),
	"path2" => array(
		"dir" => "http://woohsucat.sakura.ne.jp/Neolab/ply/ply/",
		"ply" => "quarry_simplified_3d_mesh_20170305.ply"
	),
	"dsm" => array(
		"kml" => "http://woohsucat.sakura.ne.jp/Neolab/ply/google_tiles/quarry_mosaic.kml",
		"diff" => "http://woohsucat.sakura.ne.jp/Neolab/ply/json/diff.json"
	)
);

$json = json_encode( $result ) ;

echo($json);

?>
